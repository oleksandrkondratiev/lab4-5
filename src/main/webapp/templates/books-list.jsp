<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library Management Application</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Styles -->
    <style>
        body {
            padding-top: 50px;
            text-align: center;
        }
        h1 {
            margin-bottom: 30px;
        }
        .btn {
            margin: 5px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Library Management</h1>
        <h2>
            <a href="add" class="btn btn-primary">Add New Book</a>
        </h2>
        <c:choose>
            <c:when test="${empty books}">
                <p>No books found.</p>
            </c:when>
            <c:otherwise>
                <div class="table-responsive">
                    <table class="table">
                        <caption>List of Books</caption>
                        <thead>
                            <tr>
                                <th>Genre</th>
                                <th>Author</th>
                                <th>Book Name</th>
                                <th>Factory</th>
                                <th>ISBN</th>
                                <th>Pages Count</th>
                                <th>Publish Year</th>
                                <th>Language</th>
                                <th>Insert Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="book" items="${books}">
                                <tr>
                                    <td><c:out value="${book.genre}" /></td>
                                    <td><c:out value="${book.author}" /></td>
                                    <td><c:out value="${book.bookName}" /></td>
                                    <td><c:out value="${book.factory}" /></td>
                                    <td><c:out value="${book.isbn}" /></td>
                                    <td><c:out value="${book.pagesCount}" /></td>
                                    <td><c:out value="${book.publishYear}" /></td>
                                    <td><c:out value="${book.language}" /></td>
                                    <td><c:out value="${book.insertDate}" /></td>
                                    <td>
                                        <a href="edit?id=<c:out value='${book.id}' />" class="btn btn-info">Edit</a>
                                        <a href="delete?id=<c:out value='${book.id}' />" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</body>
</html>
