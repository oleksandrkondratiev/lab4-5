<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Book</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Styles -->
    <style>
        body {
            padding-top: 50px;
            text-align: center;
        }
        h1 {
            margin-bottom: 30px;
        }
        form {
            max-width: 400px;
            margin: 0 auto;
        }
        label {
            display: block;
            margin-bottom: 5px;
            text-align: left;
        }
        input[type="text"],
        input[type="number"],
        input[type="date"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        input[type="submit"] {
            width: 100%;
            padding: 10px;
            border: none;
            border-radius: 4px;
            background-color: #007bff;
            color: white;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #0056b3;
        }
        .error {
            color: red;
            font-size: 12px;
            margin-top: 5px;
            text-align: left;
        }
    </style>
        <script>
            function validateForm() {
                var genre = document.getElementById("genre").value;
                var author = document.getElementById("author").value;
                var language = document.getElementById("language").value;

                var isbnInput = document.getElementById("isbn").value;




                if (genre.match(/\d+/)) {
                    document.getElementById("genreError").innerText = "Genre should not contain numbers.";
                    return false;
                } else {
                    document.getElementById("genreError").innerText = "";
                }

                if (author.match(/\d+/)) {
                    document.getElementById("authorError").innerText = "Author should not contain numbers.";
                    return false;
                } else {
                    document.getElementById("authorError").innerText = "";
                }

                if (language.match(/\d+/)) {
                    document.getElementById("languageError").innerText = "language should not contain numbers.";
                    return false;
                } else {
                    document.getElementById("languageError").innerText = "";
                }

                var insertDateInput = document.getElementById("insertDate").value;
                var selectedDate = new Date(insertDateInput);

                var currentDate = new Date();
                currentDate.setHours(0, 0, 0, 0); // Set current date to midnight
                selectedDate.setHours(0, 0, 0, 0); // Set selected date to midnight

                if (selectedDate.getTime() !== currentDate.getTime()) {
                    document.getElementById("insertDateError").innerText = "Date should be today's date.";
                    return false;
                } else {
                    document.getElementById("insertDateError").innerText = "";
                }

                if (isbnInput.length !== 13) {
                    document.getElementById("isbnError").innerText = "ISBN should be exactly 13 digits.";
                    return false;
                } else {
                    document.getElementById("isbnError").innerText = "";
                }

                return true;
            }
        </script>
</head>
<body>
<div class="container">
    <h1>Edit Book</h1>
    <form action="updateBook" method="post" onsubmit="return validateForm()">
        <input type="hidden" id="id" name="id" value="${book.id}">

        <label for="genre">Genre:</label>
        <input type="text" id="genre" name="genre" value="${book.genre}" required>
        <span id="genreError" class="error"></span><br>

        <label for="author">Author:</label>
        <input type="text" id="author" name="author" value="${book.author}" required>
        <span id="authorError" class="error"></span><br>

        <label for="bookName">Book Name:</label>
        <input type="text" id="bookName" name="bookName" value="${book.bookName}" required>
        <span id="bookNameError" class="error"></span><br>

        <label for="factory">Factory:</label>
        <input type="text" id="factory" name="factory" value="${book.factory}" required>
        <span id="factoryError" class="error"></span><br>

        <label for="isbn">ISBN:</label>
        <input type="number" id="isbn" name="isbn" value="${book.isbn}" required>
        <span id="isbnError" class="error"></span><br>

        <label for="pagesCount">Pages Count:</label>
        <input type="number" id="pagesCount" name="pagesCount" value="${book.pagesCount}" required>
        <span id="pagesCountError" class="error"></span><br>

        <label for="publishYear">Publish Year:</label>
        <input type="number" id="publishYear" name="publishYear" value="${book.publishYear}" required>
        <span id="publishYearError" class="error"></span><br>

        <label for="language">Language:</label>
        <input type="text" id="language" name="language" value="${book.language}" required>
        <span id="languageError" class="error"></span><br>

        <label for="insertDate">Insert Date:</label>
        <input type="date" id="insertDate" name="insertDate" value="${book.insertDate}" required>
        <span id="insertDateError" class="error"></span><br>

        <input type="submit" value="Update Book">
    </form>
</div>
</body>
</html>
