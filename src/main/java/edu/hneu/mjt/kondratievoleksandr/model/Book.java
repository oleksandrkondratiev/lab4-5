package edu.hneu.mjt.kondratievoleksandr.model;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "genre")
    private String genre;
    @Column(name = "author")
    private String author;
    @Column(name = "bookName")
    private String bookName;
    @Column(name = "factory")
    private String factory;
    @Column(name = "isbn")
    private long isbn;
    @Column(name = "pagesCount")
    private int pagesCount;
    @Column(name = "publishYear")
    private int publishYear;
    @Column(name = "language")
    private String language;
    @Column(name = "insertDate")
    @Temporal(TemporalType.DATE)
    private Date insertDate;

    public Book(){}
    public Book(int id, String genre, String author, String bookName, String factory,
                long isbn, int pagesCount, int publishYear, String language, Date insertDate) {
        this.id = id;
        this.genre = genre;
        this.author = author;
        this.bookName = bookName;
        this.factory = factory;
        this.isbn = isbn;
        this.pagesCount = pagesCount;
        this.publishYear = publishYear;
        this.language = language;
        this.insertDate = insertDate;
    }

    public Book(String genre, String author, String bookName, String factory,
                long isbn, int pagesCount, int publishYear, String language, Date insertDate) {
        this.genre = genre;
        this.author = author;
        this.bookName = bookName;
        this.factory = factory;
        this.isbn = isbn;
        this.pagesCount = pagesCount;
        this.publishYear = publishYear;
        this.language = language;
        this.insertDate = insertDate;
    }

    public int getId() {
        return id;
    }

    public String getGenre() {
        return genre;
    }

    public String getAuthor() {
        return author;
    }

    public String getBookName() {
        return bookName;
    }

    public String getFactory() {
        return factory;
    }

    public long getIsbn() {
        return isbn;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public int getPublishYear() {
        return publishYear;
    }

    public String getLanguage() {
        return language;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public void setPublishYear(int publishYear) {
        this.publishYear = publishYear;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }
}
