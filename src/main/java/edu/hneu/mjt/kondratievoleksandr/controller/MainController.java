package edu.hneu.mjt.kondratievoleksandr.controller;

import edu.hneu.mjt.kondratievoleksandr.dao.BookDAO;
import edu.hneu.mjt.kondratievoleksandr.model.Book;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet("/")
public class MainController extends HttpServlet {
    private BookDAO bookDAO;
    public void init(){
        bookDAO = new BookDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/":
                showHomePage(request, response);
                break;
            case "/add":
                showAddForm(request, response);
                break;
            case "/books-list":
                listBooks(request, response);
                break;
            case "/delete":
                deleteBook(request, response);
                break;
            case "/edit":
                editBook(request, response);
                break;
            default:
                showErrorPage(request, response);
                break;
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/addBook":
                addBook(request, response);
                break;
            case "/updateBook":
                updateBook(request, response);
                break;
        }
    }

    private void showHomePage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/home.jsp").forward(request, response);
    }
    private void showErrorPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/templates/error.jsp");
        dispatcher.forward(request, response);
    }
    private void showAddForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/add.jsp").forward(request, response);
    }
    private void addBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String genre = request.getParameter("genre");
        String author = request.getParameter("author");
        String bookName = request.getParameter("bookName");
        String factory = request.getParameter("factory");
        long isbn = Long.parseLong(request.getParameter("isbn"));
        String pagesCountStr = request.getParameter("pagesCount");
        String publishYearStr = request.getParameter("publishYear");
        String language = request.getParameter("language");
        String insertDateStr = request.getParameter("insertDate");
        int pagesCount = Integer.parseInt(pagesCountStr);
        int publishYear = Integer.parseInt(publishYearStr);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date insertDate = null;
        try {
            insertDate = sdf.parse(insertDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Book book = new Book(genre, author, bookName, factory, isbn, pagesCount, publishYear, language, insertDate);
        bookDAO.saveBook(book);
        response.sendRedirect(request.getContextPath() + "/books-list");
    }
    private void listBooks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Book> bookList = bookDAO.getAllBooks();
        request.setAttribute("books", bookList);
        request.getRequestDispatcher("/templates/books-list.jsp").forward(request, response);
    }
    private void deleteBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        bookDAO.deleteBook(id);
        response.sendRedirect(request.getContextPath() + "/books-list");
    }

    private void editBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Book book = bookDAO.getBook(id);
        request.setAttribute("book", book);
        request.getRequestDispatcher("/templates/edit.jsp").forward(request, response);
    }
    private void updateBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String genre = request.getParameter("genre");
        String author = request.getParameter("author");
        String bookName = request.getParameter("bookName");
        String factory = request.getParameter("factory");
        long isbn = Long.parseLong(request.getParameter("isbn"));
        String pagesCountStr = request.getParameter("pagesCount");
        String publishYearStr = request.getParameter("publishYear");
        String language = request.getParameter("language");
        String insertDateStr = request.getParameter("insertDate");
        int pagesCount = Integer.parseInt(pagesCountStr);
        int publishYear = Integer.parseInt(publishYearStr);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date insertDate = null;
        try {
            insertDate = sdf.parse(insertDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Book book = new Book(id, genre, author, bookName, factory, isbn, pagesCount, publishYear, language, insertDate);
        bookDAO.updateBook(book);
        response.sendRedirect(request.getContextPath() + "/books-list");
    }
}
