package edu.hneu.mjt.kondratievoleksandr.repository;

import edu.hneu.mjt.kondratievoleksandr.model.Book;

import java.util.List;

public interface BookRepository {
    void saveBook(Book book);
    void updateBook(Book book);
    void deleteBook(int id);
    Book getBook(int id);
    List<Book> getAllBooks();
}
