## Лабораторна робота 4-5

### Тема: "Servlet"

#### Виконав: Кондратьєв Олександр


##  Постановка завдання
![Скріншот1](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/task1.1.png?ref_type=heads)
![Скріншот2](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/task1.2.png?ref_type=heads)
![Скріншот3](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/task1.3.png?ref_type=heads)
![Скріншот4](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/task1.4.png?ref_type=heads)
![Скріншот5](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/task1.5.png?ref_type=heads)
---
![Скріншот4](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/1.png?ref_type=heads)
**структура проєкту**
---
![Скріншот6](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/2.png?ref_type=heads)
![Скріншот7](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/3.png?ref_type=heads)
![Скріншот8](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/4.png?ref_type=heads)
![Скріншот9](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/5.png?ref_type=heads)
![Скріншот10](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/6.png?ref_type=heads)
**MainController**
---
![Скріншот11](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/7.png?ref_type=heads)
![Скріншот12](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/8.png?ref_type=heads)
![Скріншот13](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/9.png?ref_type=heads)
**BookDAO**
---
![Скріншот14](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/10.png?ref_type=heads)
![Скріншот15](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/11.png?ref_type=heads)
![Скріншот16](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/12.png?ref_type=heads)
![Скріншот17](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/13.png?ref_type=heads)
**Book entity**
---
![Скріншот18](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/14.png?ref_type=heads)
**BookRepository**
---
![Скріншот19](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/15.png?ref_type=heads)
**HibernateUtil**
---
![Скріншот20](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/16.png?ref_type=heads)
![Скріншот21](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/17.png?ref_type=heads)
**клієнтська валідація**
---
![Скріншот22](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/18.png?ref_type=heads)
**використання шаблонізатора**
---
![Скріншот23](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/19.png?ref_type=heads)
**вітальна сторінка**
---
![Скріншот24](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/20.png?ref_type=heads)
**сторінка з книгами (наразі не додано жодної книги)**
---
![Скріншот25](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/21.png?ref_type=heads)
![Скріншот26](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/22.png?ref_type=heads)
**сторінка з додаванням книги**
---
![Скріншот27](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/23.png?ref_type=heads)
**сторінка з книгами**
---
![Скріншот28](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/24.png?ref_type=heads)
![Скріншот29](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/25.png?ref_type=heads)
![Скріншот30](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/26.png?ref_type=heads)
![Скріншот31](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/27.png?ref_type=heads)
**клієнтська валідація**
---
![Скріншот32](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/28.png?ref_type=heads)
![Скріншот33](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/29.png?ref_type=heads)
**оновлення книги**
---
![Скріншот34](https://gitlab.com/oleksandrkondratiev/lab4-5/-/raw/main/images/30.png?ref_type=heads)
**обробка 404**
---